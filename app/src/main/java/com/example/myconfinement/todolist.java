package com.example.myconfinement;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myconfinement.R;
import com.example.myconfinement.model.Item;
import com.example.myconfinement.model.Item1;
import com.example.myconfinement.model.Project;
import com.example.myconfinement.ui.TasksRecyclerAdapter;
import com.example.myconfinement.ui.TasksRecyclerAdapter1;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.realm.ObjectServerError;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.SyncConfiguration;
import io.realm.SyncCredentials;
import io.realm.SyncUser;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class todolist extends Fragment {


    public Realm realmm;
    private TasksRecyclerAdapter tasksRecyclerAdapter1;
    private ImageView image;
    private File dest;
    private static final String INSTANCE_ADDRESS = "deengineered-soft-shirt.de1a.cloud.realm.io";
    public static final String AUTH_URL = "https://" + INSTANCE_ADDRESS + "/auth";
    public static final String REALM_URL = "realms://" + INSTANCE_ADDRESS;
    TimePickerDialog picker;
    DatePickerDialog pciker2;
    int day ;
    int month ;
    int years;
    int hour;
    int minutes ;
    Date date_select;
    Calendar calendar;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_todolist, container, false);
        RecyclerView recyclerView = (RecyclerView)root.findViewById(R.id.imagegallery1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        SyncCredentials credentials = SyncCredentials.usernamePassword("test", "test", false);
        SyncUser.logInAsync(credentials, AUTH_URL, new SyncUser.Callback<SyncUser>() {
            @Override
            public void onSuccess(SyncUser user) {
                System.out.println("ok");

                //     String projectUrl = getIntent().getStringExtra(INTENT_EXTRA_PROJECT_URL);
        /*        SyncConfiguration config = SyncUser.current()
                        .createConfiguration(REALM_URL + "/~/project")
                        .fullSynchronization()
                        .initialData(realm -> {
                            Project project = new Project();
                            String userId = SyncUser.current().getIdentity();
                            project.setOwner(userId);
                            project.setName("Tasks");
                            project.setTimestamp(new Date());
                            realm.insert(project);
                        })
                        .waitForInitialRemoteData(30, TimeUnit.SECONDS)
                        .build();*/


                    Realm.getInstanceAsync(Realm.getDefaultConfiguration(), new Realm.Callback() {
                        @Override
                        public void onSuccess(Realm realm) {
                            realmm = realm;
                            Project project = realmm.where(Project.class).findFirst();
                            if (project != null) {
                                RealmResults<Item> tasks = project.getTasks().where().sort("timestamp", Sort.ASCENDING).findAllAsync();
                                tasks.addChangeListener(list -> {
                                    if (list.isEmpty()) {

                                    } else {

                                    }
                                });
                                System.out.println(tasks+"nada");
                                tasksRecyclerAdapter1 = new TasksRecyclerAdapter(tasks);
                                recyclerView.setAdapter(tasksRecyclerAdapter1);
                            }
                            else
                            {
                                System.out.println("pas ok");
                            }
                        }
                    });
              /*  Realm.getInstanceAsync(config, new Realm.Callback() {
                    @Override
                    public void onSuccess(Realm realm) {
                        realmm = realm;
                        Project project = realmm.where(Project.class).findFirst();
                        if (project != null) {
                            //            project.setTitle(project.getName());
                            RealmResults<Item> tasks = project.getTasks().where().sort("timestamp", Sort.ASCENDING).findAllAsync();
                            tasks.addChangeListener(list -> {
                                if (list.isEmpty()) {

                                } else {

                                }
                            });
                            System.out.println(tasks+"nada");
                            tasksRecyclerAdapter1 = new TasksRecyclerAdapter(tasks);
                            recyclerView.setAdapter(tasksRecyclerAdapter1);
                        } else {
                            //     setStatus("erreur");
                        }
                    }
                });*/
            }

            @Override
            public void onError(ObjectServerError error) {

                Log.e("Login error", error.toString());
            }
        });



        root.findViewById(R.id.fab).setOnClickListener(view -> {

            date_select = null;
            calendar = null;
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_task, null);
            EditText taskText = dialogView.findViewById(R.id.task);
            EditText categorie = dialogView.findViewById(R.id.editText4);
            Button btn1 = dialogView.findViewById(R.id.button);
            Button btn2 = dialogView.findViewById(R.id.button2);


            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    // time picker dialog
                    int h = 0;
                    picker = new TimePickerDialog(getContext(),
                            new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
                                    //    eText.setText(sHour + ":" + sMinute);
                                    btn1.setText("Heure:"+sHour + ":" + sMinute);
                                    hour = sHour;
                                    minutes = sMinute;
                                    System.out.println(sHour+"date");
                                    System.out.println(hour+"date");
                                    System.out.println(sMinute+"date");
                                    System.out.println(minutes+"date");

                                }
                            }, h, h, true);
                    picker.show();

                }
            });
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // date picker dialog
                    int h = 0;
                    pciker2 = new DatePickerDialog(getContext(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    day = dayOfMonth;
                                    years = year;
                                    month = monthOfYear;
                                    btn2.setText("Date :"+dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
                                    System.out.println(years+"date");
                                    System.out.println(month+"date");
                                    System.out.println(day+"date");
                                    System.out.println(hour+"date");
                                    System.out.println(minutes+"date");
                                    calendar = new GregorianCalendar(years,month,day,hour,minutes,00);
                                    date_select = calendar.getTime();

                                    System.out.println( calendar.getTime()+"rtt");
                                    //    eText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                }
                            }, h, h, h);
                    pciker2.show();

                }
            });












            new AlertDialog.Builder(getContext())
                    .setTitle("Ajouter une tâche")
                    .setView(dialogView)
                    .setPositiveButton("Ajouter", (dialog, which) -> realmm.executeTransactionAsync(realm -> {
                        EditText rt = dialogView.findViewById(R.id.task);

                        Item item = new Item();
                        item.setBody(taskText.getText().toString());
                        item.SetCategorie(categorie.getText().toString());

                        System.out.println(date_select+"  search");
                        Date today = new Date();
                        item.setTimestamp(date_select);
                        realm.where(Project.class).findFirst().getTasks().add(item);



                        try {
                            String jsonResponse;

                            URL url = new URL("https://onesignal.com/api/v1/notifications");
                            HttpURLConnection con = (HttpURLConnection)url.openConnection();
                            con.setUseCaches(false);
                            con.setDoOutput(true);
                            con.setDoInput(true);

                            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                            con.setRequestProperty("Authorization", "Basic NjMyYzcxYzAtY2UzZi00MGMyLWE4ZjctYWNiMGEzNjJlNzJm");
                            con.setRequestMethod("POST");

                            String date = String.valueOf(years)+"-"+String.valueOf(month+1)+"-"+String.valueOf(day)+" "+String.valueOf(hour)+":"+String.valueOf(minutes)+":00 GMT+0200";
                            System.out.println(date+"ananananan");
                            String strJsonBody = "{"
                                    +   "\"app_id\": \"dc2d66b5-2b68-4b33-ba2d-e01993272e54\","
                                    +   "\"included_segments\": [\"All\"],"
                                    +   "\"data\": {\"foo\": \"bar\"},"
                                    +   "\"contents\": {\"en\": \""+taskText.getText().toString()+"\"},"
                                    +   "\"send_after\": \""+date+"\""
                                    + "}";
                         //   "2015-09-24 14:00:00 GMT-0700"
                            //    +   "\"send_after\": \""+date_select+"\""

                            System.out.println("strJsonBody:\n" + strJsonBody);

                            byte[] sendBytes = strJsonBody.getBytes("UTF-8");
                            con.setFixedLengthStreamingMode(sendBytes.length);

                            OutputStream outputStream = con.getOutputStream();
                            outputStream.write(sendBytes);

                            int httpResponse = con.getResponseCode();
                            System.out.println("httpResponse: " + httpResponse);

                            if (  httpResponse >= HttpURLConnection.HTTP_OK
                                    && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
                                Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
                                jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                                scanner.close();
                            }
                            else {
                                Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
                                jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                                scanner.close();
                            }
                            System.out.println("jsonResponse:\n" + jsonResponse);

                        } catch(Throwable t) {
                            t.printStackTrace();
                        }
                    }))
                    .setNegativeButton("Annuler", null)
                    .create()
                    .show();
        });


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                String id = tasksRecyclerAdapter1.getItem(position).getItemId();
                realmm.executeTransactionAsync(realm -> {
                    Item item = realm.where(Item.class).equalTo("itemId", id).findFirst();
                    if (item != null) {
                        item.deleteFromRealm();
                    }
                });
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


        return root;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");


            int REQUEST_EXTERNAL_STORAGE = 1;
            String[] PERMISSIONS_STORAGE = {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };

            int permission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                        getActivity(),
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );
            }
            image.setImageBitmap(photo);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            //   img = Base64.encodeToString(byteArray, Base64.DEFAULT);
            Date date = Calendar.getInstance().getTime();
            String filename =  UUID.randomUUID().toString()+".png";
            File sd = Environment.getExternalStorageDirectory();
            dest = new File(sd, filename);
            System.out.println(dest);

            Bitmap bitmap = (Bitmap)data.getExtras().get("data");
            try {
                FileOutputStream out = new FileOutputStream(dest);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    private void deleteFromDatabase(final String itemName) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                Item1 item = realm.where(Item1.class).equalTo("itemId", itemName).findFirst();
                if(item != null) {
                    item.deleteFromRealm();
                }
                else{
                    System.out.println("gotnsi");
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
                Log.v("database", "Delete ok");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                Log.e("database", error.getMessage());
            }
        });

    }
    class ExampleThread extends Thread {
        int seconds;

        ExampleThread(int seconds) {
            this.seconds = seconds;
        }

        @Override
        public void run() {

            for (int i = 0; i < seconds; i++) {
                Log.d(TAG, "startThread: " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
