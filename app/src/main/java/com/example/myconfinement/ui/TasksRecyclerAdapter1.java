package com.example.myconfinement.ui;


import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiEnterpriseConfig;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myconfinement.R;
import com.example.myconfinement.model.Item;
import com.example.myconfinement.model.Item1;
import com.example.myconfinement.model.Project;
import com.example.myconfinement.take_photos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.Sort;


public class TasksRecyclerAdapter1 extends RealmRecyclerViewAdapter<Item1, TasksRecyclerAdapter1.MyViewHolder> {
    Realm realmm;
    private Context context;
    public TasksRecyclerAdapter1(OrderedRealmCollection<Item1> data) {
        super(data, true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_layout, parent, false);
        Button btn1 = itemView.findViewById(R.id.button4);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Item1 item = getItem(position);
        String id = getItem(position).getItemId();
        try {
            holder.setItem(item);
        } catch (IOException e) {
            e.printStackTrace();
        }

        holder.btn.setOnClickListener(new View.OnClickListener() {

 // photos
            @Override
            public void onClick(View view) {
                Realm.getInstanceAsync(Realm.getDefaultConfiguration(), new Realm.Callback() {
                    @Override
                    public void onSuccess(Realm realm) {
                        realmm = realm;
                        Project project = realmm.where(Project.class).findFirst();
                        if (project != null) {
                            Item1 item = realm.where(Item1.class).equalTo("itemId", id).findFirst();
                            File img=null;
                            Intent cameraIntent = new Intent(context, take_photos.class);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,img);
                            cameraIntent.putExtra("id",id);
                            context.startActivities(new Intent[]{cameraIntent});

                           // RealmResults<Item> tasks = project.getTasks().where().sort("timestamp", Sort.ASCENDING).findAllAsync();



                        }
                    }
                });
            }
        });

        holder.btn1.setOnClickListener(new View.OnClickListener() {

            // villle edit
            @Override
            public void onClick(View view) {



                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Définir ville");
                alertDialog.setMessage("");
                final EditText input = new EditText(context);
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                alertDialog.setView(input);
                alertDialog.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Realm.getInstanceAsync(Realm.getDefaultConfiguration(), new Realm.Callback() {
                            @Override
                            public void onSuccess(Realm realm) {
                                realmm = realm;
                                Project project = realmm.where(Project.class).findFirst();
                                if (project != null) {
                                    Item1 item = realm.where(Item1.class).equalTo("itemId", id).findFirst();
                                    realm.beginTransaction();
                                  item.setPlace(input.getText().toString());
                                    realm.commitTransaction();



                                }
                            }
                        });
                       System.out.println(input.getText().toString());

                    }
                });

                AlertDialog dialog = alertDialog.create();
                dialog.show();

            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView img;
        TextView textView;
        TextView cates;
        TextView dateee;
        Button btn;
        Button btn1;
        Item1 mItem;
        CheckBox checkBox;



        MyViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img);
            textView = itemView.findViewById(R.id.title);
            cates = itemView.findViewById(R.id.emp);
            dateee = itemView.findViewById(R.id.date);
            img = itemView.findViewById(R.id.imageView);
            btn = itemView.findViewById(R.id.button4);
            btn1 = itemView.findViewById(R.id.button3);
            Item1 mItem;
            context = itemView.getContext();

        }


        void setItem(Item1 item) throws IOException {
            this.mItem = item;
            this.textView.setText(item.getBody());
            // File f=new File(item.getImg());
            System.out.println(Environment.getExternalStorageDirectory());
            FileInputStream fis = new FileInputStream(new File(Environment.getExternalStorageDirectory() + File.separator +"pippo.png"));
            //    File imgFile = new  File("/storage/emulated/0/pippo.png");




            File imgFile = new  File(item.getImg());
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            //         Bitmap myBitmap = BitmapFactory.decodeFile("/storage/emulated/0/pippo.png");
            this.img = itemView.findViewById(R.id.img);
            this.img.setImageBitmap(myBitmap);

            //File f=new File(Environment.getExternalStorageDirectory().toString()+"/pippo.png");

            //    this.img.setImageBitmap(b);
            Date anan =item.getTimestamp();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");

            String strDate = sdf.format(anan);


            sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
            strDate = sdf.format(anan);


            this.dateee.setText(strDate);
            this.cates.setText(item.getPlace());
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            System.out.println(position);
            String itemId = mItem.getItemId();


//System.out.println(itemId+"       anannasznfezjfnzk");


            this.mItem.getRealm().executeTransactionAsync(realm -> {
                Item1 item = realm.where(Item1.class).equalTo("itemId", itemId).findFirst();
                if (item != null) { System.out.println(item);
                }
                else
                {
                    System.out.println("item yok");
                }
            });
        }
    }

    private void deleteFromDatabase(final String itemName) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                Item1 item = realm.where(Item1.class).equalTo("itemId", itemName).findFirst();
                if(item != null) {
                    item.deleteFromRealm();
                }
                else{
                    System.out.println("gotnsi");
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
                Log.v("database", "Delete ok");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                Log.e("database", error.getMessage());
            }
        });

    }

}
