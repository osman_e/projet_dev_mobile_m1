package com.example.myconfinement.ui;


import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.myconfinement.R;
import com.example.myconfinement.model.Item;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;


public class TasksRecyclerAdapter extends RealmRecyclerViewAdapter<Item, TasksRecyclerAdapter.MyViewHolder> {

    public TasksRecyclerAdapter(OrderedRealmCollection<Item> data) {
        super(data, true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Item item = getItem(position);
        holder.setItem(item);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        TextView cates;
        TextView dateee;

        CheckBox checkBox;
        Item mItem;

        MyViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.body);
            cates = itemView.findViewById(R.id.textView);
            dateee = itemView.findViewById(R.id.textView2);

        }

        void setItem(Item item){
            this.mItem = item;
            this.textView.setText(item.getBody());
            this.cates.setText(item.getCategorie());
            Date anan =item.getTimestamp();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");

            String strDate = sdf.format(anan);


            sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
            strDate = sdf.format(anan);


            this.dateee.setText(strDate);

        }

        @Override
        public void onClick(View v) {
            System.out.println("je click");
            String itemId = mItem.getItemId();
           this.mItem.getRealm().executeTransactionAsync(realm -> {
                Item item = realm.where(Item.class).equalTo("itemId", itemId).findFirst();
                if (item != null) {

                }
            });
        }
    }
}
