package com.example.myconfinement.ui.slideshow;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.myconfinement.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SlideshowFragment extends Fragment {

    MapView mMapView;
    private GoogleMap googleMap;
    private JSONArray eray;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_slideshow, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;


                LocationManager locManager;
                locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


                if (!locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    /** on lance notre activity (qui est une dialog) */
                    createGpsDisabledAlert();
                }

                // For showing a move to my location button
                if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                                PackageManager.PERMISSION_GRANTED) {
                    googleMap.setMyLocationEnabled(true);
                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                } else {
                    /*ActivityCompat.requestPermissions(this, new String[] {
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION);*/
                    System.out.println("je suis ici");
                    ActivityCompat.requestPermissions(getActivity(),new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }

                googleMap.setMyLocationEnabled(true);
                LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
                Location myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (myLocation == null) {
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                    String provider = lm.getBestProvider(criteria, true);
                    myLocation = lm.getLastKnownLocation(provider);
                }

                if(myLocation!=null){
                    LatLng userLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 14), 1500, null);
                }
                 // Test avec des cordonnés prédéfinis car je ne suis pas sur lyon
                String v1="45.75379945822723";
                String v2="4.859863339774901";

                // cordonne gps user
             //   v1 = Double.toString(myLocation.getLatitude());
                System.out.println(Double.toString(myLocation.getLatitude()));
                System.out.println(Double.toString(myLocation.getLongitude()));
             //   v2 = Double.toString(myLocation.getLongitude());
                //
                RequestQueue queue = Volley.newRequestQueue(getContext());

                // requete sur api avec cordonnes gps user
                // String url ="https://public.opendatasoft.com/api/records/1.0/search/?dataset=station-velov-grand-lyon&facet=name&facet=commune&facet=bonus&facet=status&facet=available&facet=availabl_1&facet=availabili&facet=availabi_1&facet=last_upd_1&geofilter.distance="+v1+"%2C"+v2+"%2C100";
                // test api
                String url = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=station-velov-grand-lyon&facet=name&facet=commune&facet=bonus&facet=status&facet=available&facet=availabl_1&facet=availabili&facet=availabi_1&facet=last_upd_1&refine.commune=Lyon+1+er";
                System.out.println(url);


                JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                        new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response) {
                                // display response
                                try {
                                    eray = response.getJSONArray("records");

                                    for (int i=0;i< eray.length();i++)
                                    {
                                        JSONObject test = (JSONObject) eray.getJSONObject(i);
                                        JSONObject test1 = (JSONObject)test.getJSONObject("fields");

                                        System.out.println(test1);
                                        System.out.println( test1.get("name"));
                                        System.out.println( test1.getJSONArray("geo_point_2d").get(0));
                                        double p = (double) test1.getJSONArray("geo_point_2d").get(0);
                                        double p1 = (double) test1.getJSONArray("geo_point_2d").get(1);
                                        LatLng lyon = new LatLng(p, p1);
                                        googleMap.addMarker(new MarkerOptions().position(lyon).title(test1.get("name").toString()));
                                    }



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.d("Response", response.toString());

                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error.Response", error.toString());
                            }
                        }
                );

// add it to the RequestQueue
                queue.add(getRequest);




                // Add a marker in Sydney and move the camera
                LatLng lyon = new LatLng(45.760630, 4.847200);
                googleMap.addMarker(new MarkerOptions().position(lyon).title("Test"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(lyon));


            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
    private void createGpsDisabledAlert() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getContext());
        localBuilder
                .setMessage("Le GPS est inactif, voulez-vous l'activer ?")
                .setCancelable(false)
                .setPositiveButton("Activer GPS ",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                showGpsOptions();
                            }
                        }
                );
        localBuilder.setNegativeButton("Ne pas l'activer ",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        paramDialogInterface.cancel();
                        getActivity().finish();
                    }
                }
        );
        localBuilder.create().show();
    }
    private void showGpsOptions() {
        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
      //  getActivity().finish();
    }
}
