package com.example.myconfinement.ui.gallery;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.myconfinement.R;

public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    private static final String TAG = "CompassTag";
    private boolean compassFound = false;
    private Compass compass;
    private ImageView arrowView;
    private TextView tvOutput, tvOutput2, tvDegree, tvMagStrength;

    private float currentAzimuth;
    float bearing = 0;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);
        arrowView = (ImageView) root.findViewById(R.id.main_image_hands);
        tvOutput = (TextView) root.findViewById(R.id.tv_output);
        tvOutput2 = (TextView) root.findViewById(R.id.tv_output2);
        tvDegree = (TextView) root.findViewById(R.id.tv_degree);
        tvMagStrength = (TextView) root.findViewById(R.id.tv_mag_strength);
         setupCompass();
        return root;
    }
    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "start compass");
        compass.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        compass.stop();
    }

    @Override
    public void onResume() {
        super.onResume();
        compass.start();
    }

    @Override
    public void onStop() {
        super.onStop();

        compass.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compass = null;
    }

    private void setupCompass() {
        compass = new Compass(getContext());
        Compass.CompassListener cl = new Compass.CompassListener() {
            @Override
            public void onNewAzimuth(float azimuth) {
                adjustArrow(azimuth);
            }

            @Override
            public void onMagField(float strength) {
                MagField(strength);
            }
        };
        compass.setListener(cl);

        if (compass != null) {
            compass.setListener(cl);
            if (compass.getStatus()) {
                compassFound = true;
            }
        }
        if (!compassFound) {
            tvOutput.setText("erreur");
        }
    }

    private void MagField(float strength) {
        tvMagStrength.setText(strength + " μT"); //milli Tesla
    }

    private void adjustArrow(float azimuth) {
        if (compass.getSensorData()) {
            float heading = azimuth;
            heading = (bearing - heading) * -1;

            Animation an = new RotateAnimation(-currentAzimuth, -heading,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            currentAzimuth = heading;

            an.setDuration(500);
            an.setRepeatCount(0);
            an.setFillAfter(true);

            arrowView.startAnimation(an);
            tvOutput.setText(String.valueOf(azimuth));
            tvOutput2.setText(String.valueOf(heading));
            showDirection(heading);
        } else {
            tvOutput.setText("erreur");
        }
    }

    private void showDirection(float degree) {
        String heading = "";
        if (degree >= 338 || degree < 23) {

            heading = "N";
        } else if (degree >= 23 && degree < 68) {

            heading = "NE";
        } else if (degree >= 68 && degree < 113) {

            heading = "E";
        } else if (degree >= 113 && degree < 158) {

            heading = "SE";
        } else if (degree >= 158 && degree < 203) {

            heading = "S";
        } else if (degree >= 203 && degree < 248) {

            heading = "SW";
        } else if (degree >= 248 && degree < 293) {

            heading = "W";
        } else if (degree >= 293 && degree < 338) {

            heading = "NW";
        }

        tvDegree.setText(Math.round(degree) + "° " + heading);
    }
}
