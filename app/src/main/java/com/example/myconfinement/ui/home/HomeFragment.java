package com.example.myconfinement.ui.home;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myconfinement.R;
import com.example.myconfinement.model.Item1;
import com.example.myconfinement.model.Project;
import com.example.myconfinement.ui.TasksRecyclerAdapter;
import com.example.myconfinement.ui.TasksRecyclerAdapter1;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.realm.ObjectServerError;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.SyncConfiguration;
import io.realm.SyncCredentials;
import io.realm.SyncUser;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class HomeFragment extends Fragment {


    public Realm realmm;
    private TasksRecyclerAdapter1 tasksRecyclerAdapter1;
    private ImageView image;
    private File dest;
    private static final String INSTANCE_ADDRESS = "deengineered-soft-shirt.de1a.cloud.realm.io";
    public static final String AUTH_URL = "https://" + INSTANCE_ADDRESS + "/auth";
    public static final String REALM_URL = "realms://" + INSTANCE_ADDRESS;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        RecyclerView recyclerView = (RecyclerView)root.findViewById(R.id.imagegallery);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));




        SyncCredentials credentials = SyncCredentials.usernamePassword("test", "test", false);
        SyncUser.logInAsync(credentials, AUTH_URL, new SyncUser.Callback<SyncUser>() {
            @Override
            public void onSuccess(SyncUser user) {
                System.out.println("ok");

                //     String projectUrl = getIntent().getStringExtra(INTENT_EXTRA_PROJECT_URL);
                SyncConfiguration config = SyncUser.current()
                        .createConfiguration(REALM_URL + "/~/project")
                        .fullSynchronization()
                        .initialData(realm -> {
                            Project project = new Project();
                            String userId = SyncUser.current().getIdentity();
                            project.setOwner(userId);
                            project.setName("Agenda");
                            project.setTimestamp(new Date());
                            realm.insert(project);
                        })
                        .waitForInitialRemoteData(30, TimeUnit.SECONDS)
                        .build();
                Realm.setDefaultConfiguration(config); // <-- here
                Realm.getInstanceAsync(config, new Realm.Callback() {
                    @Override
                    public void onSuccess(Realm realm) {
                        realmm = realm;
                        Project project = realmm.where(Project.class).findFirst();
                        if (project != null) {
                //            project.setTitle(project.getName());
                            RealmResults<Item1> tasks = project.getAgenda().where().sort("timestamp", Sort.ASCENDING).findAllAsync();
                            tasks.addChangeListener(list -> {
                                if (list.isEmpty()) {

                                } else {

                                }
                            });
                            System.out.println(tasks+"nada");
                            tasksRecyclerAdapter1 = new TasksRecyclerAdapter1(tasks);
                            recyclerView.setAdapter(tasksRecyclerAdapter1);
                        } else {
                            //     setStatus("erreur");
                        }
                    }
                });
            }

            @Override
            public void onError(ObjectServerError error) {

                Log.e("Login error", error.toString());
            }
        });
        root.findViewById(R.id.fab).setOnClickListener(view -> {


            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.agenda_task, null);
            EditText taskText = dialogView.findViewById(R.id.agenda_desc);
            Button btn1 = dialogView.findViewById(R.id.button_photos);
            image = (ImageView) dialogView.findViewById(R.id.imageView);

            btn1.setOnClickListener(new View.OnClickListener() { // heure - minute

                @Override
                public void onClick(View v) {
                    // time picker dialog

                    File img=null;
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,img);
                    startActivityForResult(cameraIntent, 1888);


                }
            });









            new AlertDialog.Builder(getContext())
                    .setTitle("Ajouter une tâche")
                    .setView(dialogView)
                    .setPositiveButton("Ajouter", (dialog, which) -> realmm.executeTransactionAsync(realm -> {
                        EditText rt = dialogView.findViewById(R.id.agenda_desc);
                        EditText rt1 = dialogView.findViewById(R.id.editText);

                        Item1 item = new Item1();
                        item.setBody(rt.getText().toString());
                        item.setImg(dest.toString());
                        item.setPlace(rt1.getText().toString());
                        Date date = Calendar.getInstance().getTime();
                        item.setTimestamp(date);
                        realm.where(Project.class).findFirst().getAgenda().add(item);
                    }))
                    .setNegativeButton("Annuler", null)
                    .create()
                    .show();
        });
        return root;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");


            int REQUEST_EXTERNAL_STORAGE = 1;
            String[] PERMISSIONS_STORAGE = {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };

            int permission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                        getActivity(),
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );
            }
            image.setImageBitmap(photo);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            //   img = Base64.encodeToString(byteArray, Base64.DEFAULT);
            Date date = Calendar.getInstance().getTime();
            String filename =  UUID.randomUUID().toString()+".png";
            File sd = Environment.getExternalStorageDirectory();
            dest = new File(sd, filename);
            System.out.println(dest);

            Bitmap bitmap = (Bitmap)data.getExtras().get("data");
            try {
                FileOutputStream out = new FileOutputStream(dest);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    private void deleteFromDatabase(final String itemName) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                Item1 item = realm.where(Item1.class).equalTo("itemId", itemName).findFirst();
                if(item != null) {
                    item.deleteFromRealm();
                }
                else{
                    System.out.println("gotnsi");
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
                Log.v("database", "Delete ok");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                Log.e("database", error.getMessage());
            }
        });

    }

}
