package com.example.myconfinement;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import com.example.myconfinement.model.Item1;
import com.example.myconfinement.model.Project;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import io.realm.Realm;

public class take_photos extends AppCompatActivity {
    private File dest;
    Realm realmm;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        id =   intent.getStringExtra("id");
        File img=null;
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,img);
        startActivityForResult(cameraIntent, 1888);


    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");


            int REQUEST_EXTERNAL_STORAGE = 1;
            String[] PERMISSIONS_STORAGE = {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };

            int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                        this,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            //   img = Base64.encodeToString(byteArray, Base64.DEFAULT);
            Date date = Calendar.getInstance().getTime();
            String filename =  UUID.randomUUID().toString()+".png";
            File sd = Environment.getExternalStorageDirectory();
            dest = new File(sd, filename);
            System.out.println(dest);

            Bitmap bitmap = (Bitmap)data.getExtras().get("data");
            try {
                FileOutputStream out = new FileOutputStream(dest);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("erreur");
            }
            System.out.println(id);
            Realm.getInstanceAsync(Realm.getDefaultConfiguration(), new Realm.Callback() {
                @Override
                public void onSuccess(Realm realm) {
                    realmm = realm;
                    Project project = realmm.where(Project.class).findFirst();
                    if (project != null) {
                        Item1 item = realm.where(Item1.class).equalTo("itemId", id).findFirst();

                        System.out.println(dest.getAbsolutePath());
                        // RealmResults<Item> tasks = project.getTasks().where().sort("timestamp", Sort.ASCENDING).findAllAsync();
                  realm.beginTransaction();
                        item.setImg(dest.getAbsolutePath());
                        realm.commitTransaction();
                        System.out.println(item+"nada");
                        finish();

                    }
                }
            });
        }
    }
}
